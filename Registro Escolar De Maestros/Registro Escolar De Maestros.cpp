// UNIVERSIDAD MARIANO GALVEZ - VLADIMIR MUNRAYOS - 1190-14-1874
// PROGRAMA PARA REGISTRO DE UNA ESCUELA.

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <iostream>

struct registro{
	char nombre[30];
	char sexo[10];
	float edad;
}profesores[6];

int i, m, e, e1, p, j, g, pr = 0, pr2 = 0;
float suma, promedio, pm2, pm3;
int resultado, resultado1;

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "******* BIEVENIDO AL REGISTRO ************\n";
	cout << "Porfavor ingrese los datos de 6 usuarios para el registro de profesores\n";
	for (i = 0; i < 6; i++){
		cout << "Ingrese nombre del usuario\t";
		cin >> profesores[i].nombre;
		cout << "Ingrese edad del usuario\t";
		cin >> profesores[i].edad;
		cout << "Ingrese tipo de sexo del usuario\t";
		cin >> profesores[i].sexo;
		cout << "------------------------------------\n";
	}
	cout << "Datos Capturados correctamente\n";
	_getch();
	system("cls");
	int opr;
	menu:
	cout << "1. Edad promedio del grupo de profesores\n";
	cout << "2. Nombre del profesor mas joven del grupo\n";
	cout << "3. Nombre del profesor de mas edad del grupo\n";
	cout << "4. Numero de profesoras con edad mayor al promedio\n";
	cout << "5. Numero de profesores con edad menor al promedio\n";
	cout << "6. Salir\n";
	scanf_s("%d", &opr);
	system("cls");
	if (opr == 1){
		for (m = 0; m < 6; m++){
			cout << "Edad del profesor: " << profesores[m].edad << "\n";
			cout << "----------------------\n";
		}
		
		suma = profesores[0].edad + profesores[1].edad + profesores[2].edad + profesores[3].edad + profesores[4].edad + profesores[5].edad;
		promedio = suma / 6;
		cout << "La suma de los profesores es: " << suma << endl;
		cout << "el promedio de los profesores es:" << promedio;
		_getch();
		system("cls");
		goto menu;
	}
	if (opr == 2){
		for (e = 0; e < 6; e++){
			cout << "Nombre del profesor: " << profesores[e].nombre << "\n";
			cout << "------------------------\n";
		}
		if ((profesores[0].edad < profesores[1].edad) && (profesores[0].edad < profesores[2].edad) && (profesores[0].edad < profesores[3].edad) && (profesores[0].edad < profesores[4].edad) && (profesores[0].edad < profesores[5].edad)){
			resultado = 1;
		}
		else
			if ((profesores[1].edad < profesores[0].edad) && (profesores[1].edad < profesores[2].edad) && (profesores[1].edad < profesores[3].edad) && (profesores[1].edad < profesores[4].edad) && (profesores[1].edad < profesores[5].edad)){
				resultado = 2;
			}
			else
				if ((profesores[2].edad < profesores[0].edad) && (profesores[2].edad < profesores[1].edad) && (profesores[2].edad < profesores[3].edad) && (profesores[2].edad < profesores[4].edad) && (profesores[2].edad < profesores[5].edad)){
					resultado = 3;
				}
				else
					if ((profesores[3].edad < profesores[0].edad) && (profesores[3].edad < profesores[1].edad) && (profesores[3].edad < profesores[2].edad) && (profesores[3].edad < profesores[4].edad) && (profesores[3].edad < profesores[5].edad)){
						resultado = 4;
					}
					else
						if ((profesores[4].edad < profesores[0].edad) && (profesores[4].edad < profesores[1].edad) && (profesores[4].edad < profesores[2].edad) && (profesores[4].edad < profesores[3].edad) && (profesores[4].edad < profesores[5].edad)){
							resultado = 5;
						}
						else
							if ((profesores[5].edad < profesores[0].edad) && (profesores[5].edad < profesores[1].edad) && (profesores[5].edad < profesores[2].edad) && (profesores[5].edad < profesores[3].edad) && (profesores[5].edad < profesores[4].edad)){
								resultado = 6;
							}
		switch (resultado){
		case 1:
			cout << "\n";
			cout << "el profesor mas joven es: " << profesores[0].nombre;
			break;
		case 2:
			cout << "\n";
			cout << "el profesor mas joven es: " << profesores[1].nombre;
			break;
		case 3:
			cout << "\n";
			cout << "el profesor mas joven es: " << profesores[2].nombre;
			break;
		case 4: 
			cout << "\n";
			cout << "el profesor mas joven es: " << profesores[3].nombre;
			break;
		case 5: 
			cout << "\n";
			cout << "el profesor mas joven es: " << profesores[4].nombre;
			break;
		case 6:
			cout << "\n";
			cout << "el profesor mas joven es: " << profesores[5].nombre;
			break;
		default:
			cout << "\n";
			cout << "todos los profesores tienen la misma edad";
			break;
		}
		_getch();
		system("cls");
		goto menu;
     }
	
	if (opr == 3){
		for (e1 = 0; e1 < 6; e1++){
			cout << "Nombre del profesor: " << profesores[e1].nombre << "\n";
			cout << "------------------------\n";
		}
		if ((profesores[0].edad > profesores[1].edad) && (profesores[0].edad > profesores[2].edad) && (profesores[0].edad > profesores[3].edad) && (profesores[0].edad > profesores[4].edad) && (profesores[0].edad > profesores[5].edad)){
			resultado1 = 1;
		}
		else
			if ((profesores[1].edad > profesores[0].edad) && (profesores[1].edad > profesores[2].edad) && (profesores[1].edad > profesores[3].edad) && (profesores[1].edad > profesores[4].edad) && (profesores[1].edad > profesores[5].edad)){
				resultado1 = 2;
			}
			else
				if ((profesores[2].edad > profesores[0].edad) && (profesores[2].edad > profesores[1].edad) && (profesores[2].edad > profesores[3].edad) && (profesores[2].edad > profesores[4].edad) && (profesores[2].edad > profesores[5].edad)){
					resultado1 = 3;
				}
				else
					if ((profesores[3].edad > profesores[0].edad) && (profesores[3].edad > profesores[1].edad) && (profesores[3].edad > profesores[2].edad) && (profesores[3].edad > profesores[4].edad) && (profesores[3].edad > profesores[5].edad)){
						resultado1 = 4;
					}
					else
						if ((profesores[4].edad > profesores[0].edad) && (profesores[4].edad > profesores[1].edad) && (profesores[4].edad > profesores[2].edad) && (profesores[4].edad > profesores[3].edad) && (profesores[4].edad > profesores[5].edad)){
							resultado1 = 5;
						}
						else
							if ((profesores[5].edad > profesores[0].edad) && (profesores[5].edad > profesores[1].edad) && (profesores[5].edad > profesores[2].edad) && (profesores[5].edad > profesores[3].edad) && (profesores[5].edad > profesores[4].edad)){
								resultado1 = 6;
							}
		switch (resultado1){
		case 1:
			cout << "\n";
			cout << "el profesor de mas edad es: " << profesores[0].nombre;
			break;
		case 2:
			cout << "\n";
			cout << "el profesor de mas edad es: " << profesores[1].nombre;
			break;
		case 3:
			cout << "\n";
			cout << "el profesor de mas edad es: " << profesores[2].nombre;
			break;
		case 4:
			cout << "\n";
			cout << "el profesor de mas edad es: " << profesores[3].nombre;
			break;
		case 5:
			cout << "\n";
			cout << "el profesor de mas edad es: " << profesores[4].nombre;
			break;
		case 6:
			cout << "\n";
			cout << "el profesor de mas edad es: " << profesores[5].nombre;
			break;
		default:
			cout << "\n";
			cout << "todos los profesores tienen la misma edad";
			break;
		}
		_getch();
		system("cls");
		goto menu;
	}
	if (opr== 4){
		char fem[]="femenino";
		for (m = 0; m < 6; m++){
			cout << "Edad del profesor: " << profesores[m].edad << "\n";
			cout << "----------------------\n";
		}

		suma = profesores[0].edad + profesores[1].edad + profesores[2].edad + profesores[3].edad + profesores[4].edad + profesores[5].edad;
		promedio = suma / 6;
		cout << "La suma de los profesores es: " << suma << endl;
		cout << "el promedio de los profesores es:" << promedio << endl;
		for (int j = 0; j < 6; j++){

			if (profesores[j].edad > promedio && strcmp (profesores[j].sexo,"femenino")!= 0){
				pm2 = pr++;
				
			}
		}
                 
		 cout << "El numero de profesoras mayores al promedio es: " << pm2 << endl;

		  _getch();
		  system("cls");
		  goto menu;
		}
	if (opr == 5){
		char mas[] = "masculino";
		for (m = 0; m < 6; m++){
			cout << "Edad del profesor: " << profesores[m].edad << "\n";
			cout << "----------------------\n";
		}

		suma = profesores[0].edad + profesores[1].edad + profesores[2].edad + profesores[3].edad + profesores[4].edad + profesores[5].edad;
		promedio = suma / 6;
		cout << "La suma de los profesores es: " << suma << endl;
		cout << "el promedio de los profesores es:" << promedio << endl;
		for (int g = 0; g < 6; g++){

			if (profesores[g].edad < promedio && strcmp(profesores[g].sexo, "masculino") != 0) {
				pm3 = pr2++;
			}
		}

		cout << "El numero de profesores menores al promedio es: " << pm3 << endl;
		_getch();
		system("cls");
		goto menu;
	}
		
	}

